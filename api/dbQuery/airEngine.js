/*
airEngine: wraps the airtable.js library for use by other query classes,
exposing the db option
*/
const airtable = require('airtable');

const creds = require('schluessel');
const airTableApiKey = creds.airTable.apiKey;
const airTableBaseID = creds.airTable.BaseID;

airtable.configure({
    apiKey: airTableApiKey,
    endpointUrl: "https://api.airtable.com"
});

const engine = airtable.base(airTableBaseID);

module.exports = engine;
