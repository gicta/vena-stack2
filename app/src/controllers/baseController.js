
export class baseController {
  constructor(apiBaseUrl) {
    this.apiBaseUrl = apiBaseUrl;
  }

  async apiGet(endpoint) {
    const targetUrl = this.urlJoin(this.apiBaseUrl, endpoint);

    const res = await fetch(
      targetUrl ,
      {
        method: "GET",
        headers: {
          "Content-Type" : "application/json; charset=utf-8"
        }
      }
    );
    const result = await res.json();
    return result;
  }

  /**
  * Uses javascript fetch(), as a POST, with an application/json Content-Type to send a JSON
  * payload to an API endpoint
  * @param  {String} endPoint The endpoint (appended to apiBaseUrl) we are posting to
  * @param  {String} jsonPayload The already JSON.stringify()'d version of the payload expected by the endpoint 
  * @return {Object} the response from the API endpoint
  */
  async apiPost(endPoint, jsonPayload) {
    const targetUrl = await this.urlJoin(this.apiBaseUrl, endPoint);

    const res = await fetch(
      targetUrl,
      {
        method: "POST",
        headers: {
          "Content-Type" : "application/json; charset=utf-8"
        },
        body: jsonPayload
      }
    );
    const result = await res.json();
    return result;
  }

  //todo broader utility lib?
  async urlJoin(...args) {
    let result = "";
    //debug: console.log(`urlJoin: args ${JSON.stringify(args)}`);
    for(var i = 0; i < args.length; i++){
      if( args[i] == null) {
        //debug: console.log(`urlJoin: arg ${i} is null`);
        continue;
      }

      if(result.endsWith('/')) {
        if ( String(args[i]).startsWith('/') ) {
          result += String(args[i]).slice(1);
          //debug: console.log(`urlJoin: result/ /arg yields ${result}`);
        } else {
          result += String(args[i]);
          //debug: console.log(`urlJoin: result/ arg yields ${result}`);
        }
      } else {
        if ( i==0 || String(args[i]).startsWith('/') ) {
          result += String(args[i]);
          //debug: console.log(`urlJoin: result /arg yields ${result}`);
        } else {
          result += '/' + String(args[i]);
          //debug: console.log(`urlJoin: result arg yields ${result}`);
        }
      }
    }//for
    //debug: console.log(`urlJoin is returning ${result}`);
    return result;
    /*
    got this URL joiner from https://www.w3resource.com/javascript-exercises/fundamental/javascript-fundamental-exercise-55.php
    .replace(/[\/]+/g, '/')
    .replace(/^(.+):\//, '$1://')
    .replace(/^file:/, 'file:/')
    .replace(/\/(\?|&|#[^!])/g, '$1')
    .replace(/\?/g, '&')
    .replace('&', '?');
    maybe it works but I wanted to understand why... 40 minutes later trying to get through regex using https://regex101.com/
    (an awesome utility) and dozen searches elsewhere, it regex still remains cryptic to me. True, the above code is also not 
    all that nice, but it does the core thing I wanted. . . ensure there is a slash if one is missing, makes sure there are no double slashes,
    but keeps the https:// in place. Dunzo!
    */
  }
}