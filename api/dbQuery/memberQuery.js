const { json } = require('body-parser');
const engine = require('./airEngine');

class Member {
  constructor(id, login, email, lastName, firstName) {
    this.id = id;
    this.login = login;
    this.email = email;
  }
}

async function authenticateMember(login, password) {
  try {
    const result = await engine('member_login').select({
        maxRecords:33,
        view: 'Grid view',
        fields: ['login'],
        filterByFormula: `AND({login} = "${login}", {pass} = "${password}")`
    }).firstPage();

    //debug:console.log(`results: ${JSON.stringify(result)}`);

    if(result == null) {
      console.log('DENY: result is null');
      return false;
    }
    if(result.length == 0) {
      console.log(`DENY: no results matched`);
      return false;  
    }
    if(result.length > 1 ) {
      console.log(`ERROR: more than one result matched. wut?`);
      return false;  
    }
    if(result[0].fields.login == login) {
      console.log(`ALLOW: results[0].login was returned. User has authenticated`);
      return true; 
    }
    console.log(`DENY: results[0].login does not match. e.g login [${login}] doesn't match results [${result[0].fields.login}]`);
    return true; 
  } catch(e) {
    console.log(`Exception:${e}`);
    throw e;
  } 
}

async function getAllMembers() {
  try {
    const members = await engine('member_login').select({
        maxRecords:33,
        view: 'Grid view',
        fields: ['login','email'],
        filterByFormula: 'NOT({login} = "")'
    }).firstPage();
    
    const scrubbedMembers = await convertMemberData(members);
    return scrubbedMembers;

  } catch(e) {
    console.log(`Exception:${e}`);
    throw e;
  } 
}

async function convertMemberData(members) {

  console.log('userQuery.convertMemberData() function is running');
  if(members == null) {
    console.log('userQuery.convertMemberData() member object is null or undefined - returning []');    
    return []; //we got no data
  }
  if(!('length' in members)) {
    console.log('userQuery.convertMemberData() member object has no length property - returning []');    
    return []; //we got no data
  }
  if(members.length == 0) {
    console.log('userQuery.convertMemberData() no member records - returning []');    
    return []; //we got no data
  }
  if(members.length == 1 && members[0].fields.length == 0) {
    console.log('userQuery.convertMemberData() member records have no fields - returning []');
    return []; //we got no data
  }

  var scrubbedMembers = [];
  var scrubbedMember = null;
  for(var i=0; i < members.length; i++)
  {
    scrubbedMember = new Member(members[i].id, members[i].fields.login, members[i].fields.email);
    scrubbedMembers.push(scrubbedMember);
  }
  return scrubbedMembers;
}

/* 
  I am keeping this as a memory
  Magically today I don't need the promise all crap and things are behaving normally
  But I swear on my uncle's Fred box, this thing behaved exactly as commented
  and then I put in this question  
  https://stackoverflow.com/questions/71052733/i-fixed-my-async-await-thing-but-pretty-sure-i-are-doing-it-wrong
  and while trying to confirm something, I ended up not needing the promise.all 
  WHY? 
  HOW?
  My ego will not let me drop this
  I'll be fine
  blorg
*/
async function old_getAllUsers() {
  try {
    console.log('userQuery.getAllUsers() engine.select');
    const members = await engine('member_login').select({
        maxRecords:33,
        view: 'Grid view',
        fields: ['login','email'],
        filterByFormula: 'NOT({login} = "")'
    }).firstPage(); //<-- result of this is still a promise
    
    //console.log('await members')
    //await Promise.all([members]);//<--now we get an Object (for reals)
    //var members2 = [ new Promise( ()=>{return true;}), new Promise(()=>{return true;})];
    console.log(`await processed. members=${members}`);

    console.log('await scrubbed members');
    const scrubbedMembers = await convertMemberData(members); //<--returns a promise
    //await Promise.all([scrubbedMembers]);//<--now we get an Object
    console.log(`await processed. scrubbed members=${scrubbedMembers}`);
    return scrubbedMembers;

  } catch(e) {
    console.log(`Exception:${e}`);
  } 
}

module.exports = {
  authenticateMember,
  getAllMembers
}

